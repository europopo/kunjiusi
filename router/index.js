const router = require('express').Router();
const queryTop = require('../data/queryTop.json');
const queryList = require('../data/queryList.json');
const querySortList = require('../data/querySortList.json');
const queryDetail = require('../data/queryDetail.json');
const queryCart = require('../data/queryCart.json');

router.get('/', (req, res)=> {
    res.send('hello')
})

router.get('/queryTop', (req, res)=>{
    res.send(queryTop);
})

router.get('/queryList', (req, res)=>{
    res.send(queryList);
})

router.get('/querySortList', (req, res)=>{
    res.send(querySortList);
})

router.get('/queryDetail', (req, res)=>{
    res.send(queryDetail);
})

router.get('/queryCart', (req, res)=>{
    res.send(queryCart);
})

module.exports = router;